import os
import sys

sys.path.insert(0, os.path.abspath("../../"))

from os import path

here = path.abspath(path.dirname(__file__))
with open(
    path.join(here, "../../", "parametrization_cookbook", "__init__.py"),
    encoding="utf-8",
) as f:
    ast = compile(f.read(), "__init__.py", "exec")
    fake_global = {"__name__": "__main__"}
    try:
        exec(ast, fake_global)
    except (SystemError, ImportError) as e:
        print("System error")

    version = fake_global["__version__"]
    release = version


# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "Parametrization Cookbook"
copyright = "2022, Jean-Benoist Leger"
author = "Jean-Benoist Leger"
release = "0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.napoleon",
    "sphinx_rtd_theme",
]

templates_path = ["_templates"]
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
html_css_files = [
    "custom.css",
]
