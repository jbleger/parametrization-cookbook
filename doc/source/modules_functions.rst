API documentation -- Elementary functions
=========================================

.. toctree::
   :maxdepth: 4

   parametrization_cookbook.functions.numpy
   parametrization_cookbook.functions.jax
   parametrization_cookbook.functions.torch
