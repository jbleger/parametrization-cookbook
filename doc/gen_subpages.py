#!/usr/bin/env python3

classpage = """
.. _parametrization_cookbook.{back}.{cla}:

{cla}
{xcla}

.. autoclass:: parametrization_cookbook.{back}.{cla}
   :members:
   :inherited-members:
   :special-members: __init__
   :undoc-members:
   :show-inheritance:
"""

classes = (
    "Real",
    "RealPositive",
    "RealNegative",
    "RealLowerBounded",
    "RealUpperBounded",
    "RealBounded01",
    "RealBounded",
    "VectorSimplex",
    "VectorSphere",
    "VectorHalfSphere",
    "VectorBall",
    "MatrixDiag",
    "MatrixDiagPosDef",
    "MatrixSym",
    "MatrixSymPosDef",
    "MatrixCorrelation",
    "Tuple",
    "NamedTuple",
)
backends = (
    "numpy",
    "jax",
    "torch",
)

from os import path

here = path.abspath(path.dirname(__file__))

if __name__ == "__main__":

    for back in backends:
        for cla in classes:
            open(
                f"{here}/source/generated_parametrization_cookbook.{back}.{cla}.rst",
                "w",
            ).write(classpage.format(back=back, cla=cla, xcla="=" * (len(cla))))
