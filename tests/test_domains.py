import itertools
import functools
import operator

import pytest

import numpy as np
import jax.numpy as jnp
import torch

from parametrization_cookbook import numpy as pcnp
from parametrization_cookbook import jax as pcjax
from parametrization_cookbook import torch as pctorch

from .seq import reals_points

eps = 1e-6

SCALES = (1 / 1.5, 1.0, 1.5)

MODULES = (
    (pcnp, lambda x: x),
    (pcnp, np.array),
    (pcjax, jnp.array),
    (pctorch, torch.tensor),
)

from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence


@pytest.mark.parametrize(
    ("pcclass", "rscale", "scale"), itertools.product(MODULES, SCALES, SCALES)
)
def test_r2p_real(profile, pcclass, rscale, scale):
    pcclass, array = pcclass
    p = pcclass.Real(scale=scale)
    for r in reals_points(log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)


@pytest.mark.parametrize(("pcclass", "scale"), itertools.product(MODULES, SCALES))
def test_p2r_real(profile, pcclass, scale):
    pcclass, array = pcclass
    p = pcclass.Real(scale=scale)
    for v in (array(x) for x in np.linspace(-10, 10, profile.P2R_N)):
        x = p.params_to_reals1d(v)
        assert not np.isnan(x) and not np.isinf(x), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "rscale", "scale"), itertools.product(MODULES, SCALES, SCALES)
)
def test_r2p_real_positive(profile, pcclass, rscale, scale):
    pcclass, array = pcclass
    p = pcclass.RealPositive(scale=scale)
    for r in reals_points(log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v > 0, f"r={r}"


@pytest.mark.parametrize(("pcclass", "scale"), itertools.product(MODULES, SCALES))
def test_p2r_real_positive(profile, pcclass, scale):
    pcclass, array = pcclass
    p = pcclass.RealPositive(scale=scale)
    for v in (array(x) for x in np.exp(np.linspace(-10, 10, profile.P2R_N))):
        x = p.params_to_reals1d(v)
        assert not np.isnan(x) and not np.isinf(x), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "rscale", "scale"), itertools.product(MODULES, SCALES, SCALES)
)
def test_r2p_real_negative(profile, pcclass, rscale, scale):
    pcclass, array = pcclass
    p = pcclass.RealNegative(scale=scale)
    for r in reals_points(log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v < 0, f"r={r}"


@pytest.mark.parametrize(("pcclass", "scale"), itertools.product(MODULES, SCALES))
def test_p2r_real_negative(profile, pcclass, scale):
    pcclass, array = pcclass
    p = pcclass.RealNegative(scale=scale)
    for v in (array(x) for x in -np.exp(np.linspace(-10, 10, profile.P2R_N))):
        x = p.params_to_reals1d(v)
        assert not np.isnan(x) and not np.isinf(x), f"v={v}"


ONE_BOUNDS = (-10, 10)


@pytest.mark.parametrize(
    ("pcclass", "rscale", "scale", "bound"),
    itertools.product(MODULES, SCALES, SCALES, ONE_BOUNDS),
)
def test_r2p_real_lower_bounded(profile, pcclass, rscale, scale, bound):
    pcclass, array = pcclass
    p = pcclass.RealLowerBounded(bound=bound, scale=scale)
    for r in reals_points(log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert bound - eps < v, f"r={r}"


@pytest.mark.parametrize(
    ("pcclass", "scale", "bound"), itertools.product(MODULES, SCALES, ONE_BOUNDS)
)
def test_p2r_real_lower_bounded(profile, pcclass, scale, bound):
    pcclass, array = pcclass
    p = pcclass.RealLowerBounded(bound=bound, scale=scale)
    for v in (array(x) for x in (bound + np.exp(np.linspace(-5, 5, profile.P2R_N)))):
        x = p.params_to_reals1d(v)
        assert not np.isnan(x) and not np.isinf(x), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "rscale", "scale", "bound"),
    itertools.product(MODULES, SCALES, SCALES, ONE_BOUNDS),
)
def test_r2p_real_upper_bounded(profile, pcclass, rscale, scale, bound):
    pcclass, array = pcclass
    p = pcclass.RealUpperBounded(bound=bound, scale=scale)
    for r in reals_points(log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v < bound + eps, f"r={r}"


@pytest.mark.parametrize(
    ("pcclass", "scale", "bound"), itertools.product(MODULES, SCALES, ONE_BOUNDS)
)
def test_p2r_real_upper_bounded(profile, pcclass, scale, bound):
    pcclass, array = pcclass
    p = pcclass.RealUpperBounded(bound=bound, scale=scale)
    for v in (array(x) for x in (bound - np.exp(np.linspace(-5, 5, profile.P2R_N)))):
        x = p.params_to_reals1d(v)
        assert not np.isnan(x) and not np.isinf(x), f"v={v}"


@pytest.mark.parametrize(("pcclass", "rscale"), itertools.product(MODULES, SCALES))
def test_r2p_real_bounded01(profile, pcclass, rscale):
    pcclass, array = pcclass
    p = pcclass.RealBounded01()
    for r in reals_points(log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert 0 < v < 1 + eps, f"r={r}"


@pytest.mark.parametrize("pcclass", MODULES)
def test_p2r_real_bounded01(profile, pcclass):
    pcclass, array = pcclass
    p = pcclass.RealBounded01()
    for v in (
        array(x) for x in (1 / (1 + np.exp(np.linspace(-10, 10, profile.P2R_N))))
    ):
        x = p.params_to_reals1d(v)
        assert not np.isnan(x) and not np.isinf(x), f"v={v}"


TWO_BOUNDS = ((-10, -5), (-5, 5), (5, 10))


@pytest.mark.parametrize(
    ("pcclass", "rscale", "bounds"),
    itertools.product(MODULES, SCALES, TWO_BOUNDS),
)
def test_r2p_real_bounded(profile, pcclass, rscale, bounds):
    pcclass, array = pcclass
    p = pcclass.RealBounded(bound_lower=bounds[0], bound_upper=bounds[1])
    for r in reals_points(log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert bounds[0] - eps < v < bounds[1] + eps, f"r={r}"


@pytest.mark.parametrize(("pcclass", "bounds"), itertools.product(MODULES, TWO_BOUNDS))
def test_p2r_real_bounded(profile, pcclass, bounds):
    pcclass, array = pcclass
    p = pcclass.RealBounded(bound_lower=bounds[0], bound_upper=bounds[1])
    for v in (
        array(x)
        for x in (
            bounds[0]
            + (bounds[1] - bounds[0])
            / (1 + np.exp(np.linspace(-10, 10, profile.P2R_N)))
        )
    ):
        x = p.params_to_reals1d(v)
        assert not np.isnan(x) and not np.isinf(x), f"v={v}"


VECTOR_DIMS = (
    1,
    4,
    16,
    64,
)

VECTOR_DIMS_BALL = (
    2,
    4,
    16,
    32,
)


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, VECTOR_DIMS, SCALES)
)
def test_r2p_vector_simplex(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.VectorSimplex(dim=dim)
    for r in reals_points(shape=(dim,), log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim + 1,), f"r={r}"
        assert (v >= 0).all(), f"r={r}"
        assert np.isclose(v.sum(), 1.0), f"r={r}"


@pytest.mark.parametrize(("pcclass", "dim"), itertools.product(MODULES, VECTOR_DIMS))
def test_p2r_vector_simplex(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.VectorSimplex(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    for i in range(profile.P2R_N):
        v = rs.gamma(1, size=dim + 1)
        v /= v.sum()
        v = array(v)

        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, VECTOR_DIMS, SCALES)
)
def test_r2p_vector_sphere(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.VectorSphere(dim=dim)
    for r in reals_points(shape=(dim,), log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim + 1,), f"r={r}"
        assert np.isclose((v**2).sum(), 1.0), f"r={r}"


@pytest.mark.parametrize(("pcclass", "dim"), itertools.product(MODULES, VECTOR_DIMS))
def test_p2r_vector_sphere(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.VectorSphere(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    for i in range(profile.P2R_N):
        v = rs.normal(size=dim + 1)
        v /= (v**2).sum() ** 0.5
        v = array(v)

        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, VECTOR_DIMS, SCALES)
)
def test_r2p_vector_half_sphere(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.VectorHalfSphere(dim=dim)
    for r in reals_points(shape=(dim,), log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim + 1,), f"r={r}"
        assert np.isclose((v**2).sum(), 1.0), f"r={r}"
        assert v[-1] > 0, f"r={r}"


@pytest.mark.parametrize(("pcclass", "dim"), itertools.product(MODULES, VECTOR_DIMS))
def test_p2r_vector_half_sphere(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.VectorSphere(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    for i in range(profile.P2R_N):
        v = rs.normal(size=dim + 1)
        v /= (v**2).sum() ** 0.5
        v[-1] = np.abs(v[-1])
        v = array(v)

        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, VECTOR_DIMS_BALL, SCALES)
)
def test_r2p_vector_ball(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.VectorBall(dim=dim)
    for r in reals_points(shape=(dim,), log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim,), f"r={r}"
        assert (v**2).sum() < 1.0 + eps, f"r={r}"


@pytest.mark.parametrize(
    ("pcclass", "dim"), itertools.product(MODULES, VECTOR_DIMS_BALL)
)
def test_p2r_vector_ball(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.VectorBall(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    for i in range(profile.P2R_N):
        v = rs.normal(size=dim)
        v /= (v**2).sum() ** 0.5
        v *= rs.uniform() ** (1 / dim)
        v = array(v)

        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


MATRIX_SIZES = (
    1,
    2,
    4,
    8,
    16,
)

MATRIX_SIZES_CORR = (
    2,
    4,
    8,
    16,
)


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, MATRIX_SIZES, SCALES)
)
def test_r2p_matrix_diag(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.MatrixDiag(dim=dim)
    idxs = np.arange(dim)
    non_diag_idxs = np.nonzero(idxs[:, None] != idxs[None, :])
    for r in reals_points(shape=(dim,), log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim, dim), f"r={r}"
        assert (v[non_diag_idxs] == 0).all(), f"r={r}"


@pytest.mark.parametrize(("pcclass", "dim"), itertools.product(MODULES, MATRIX_SIZES))
def test_p2r_matrix_diag(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.MatrixDiag(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    p = pcclass.MatrixDiag(dim=dim)
    for i in range(profile.P2R_N):
        v = np.diag(rs.normal(size=dim))
        v = array(v)
        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, MATRIX_SIZES, SCALES)
)
def test_r2p_matrix_diag_pos_def(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.MatrixDiagPosDef(dim=dim)
    idxs = np.arange(dim)
    non_diag_idxs = np.nonzero(idxs[:, None] != idxs[None, :])
    diag_idxs = np.nonzero(idxs[:, None] == idxs[None, :])
    for r in reals_points(shape=(dim,), log2n=profile.R2P_LOG2N, wrap=array):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim, dim), f"r={r}"
        assert (v[diag_idxs] > 0).all(), f"r={r}"
        assert (v[non_diag_idxs] == 0).all(), f"r={r}"


@pytest.mark.parametrize(("pcclass", "dim"), itertools.product(MODULES, MATRIX_SIZES))
def test_p2r_matrix_diag_pos_def(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.MatrixDiagPosDef(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    p = pcclass.MatrixDiagPosDef(dim=dim)
    for i in range(profile.P2R_N):
        v = np.diag(rs.normal(size=dim) ** 2)
        v = array(v)
        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, MATRIX_SIZES, SCALES)
)
def test_r2p_matrix_sym(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.MatrixSym(dim=dim)
    for r in reals_points(
        shape=(dim * (dim + 1) // 2,), log2n=profile.R2P_LOG2N, wrap=array
    ):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim, dim), f"r={r}"
        assert np.abs(v - v.T).max() < eps, f"r={r}"


@pytest.mark.parametrize(("pcclass", "dim"), itertools.product(MODULES, MATRIX_SIZES))
def test_p2r_matrix_sym(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.MatrixDiag(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    p = pcclass.MatrixSym(dim=dim)
    for i in range(profile.P2R_N):
        v = rs.normal(size=(dim, dim))
        v += v.T
        v = array(v)
        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim * (dim + 1) // 2
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, MATRIX_SIZES, SCALES)
)
def test_r2p_matrix_sym_pos_def(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.MatrixSymPosDef(dim=dim)
    for r in reals_points(
        shape=(dim * (dim + 1) // 2,), log2n=profile.R2P_LOG2N, wrap=array
    ):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim, dim), f"r={r}"
        assert np.abs(v - v.T).max() < eps, f"r={r}"
        assert (np.linalg.eigh(v)[0]).min() > -dim * eps


@pytest.mark.parametrize(("pcclass", "dim"), itertools.product(MODULES, MATRIX_SIZES))
def test_p2r_matrix_sym_pos_def(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.MatrixDiag(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    p = pcclass.MatrixSymPosDef(dim=dim)
    for i in range(profile.P2R_N):
        v = rs.normal(size=(dim, dim))
        v = v @ v.T
        v = array(v)
        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim * (dim + 1) // 2
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"


@pytest.mark.parametrize(
    ("pcclass", "dim", "rscale"), itertools.product(MODULES, MATRIX_SIZES_CORR, SCALES)
)
def test_r2p_matrix_corr(profile, pcclass, dim, rscale):
    pcclass, array = pcclass
    p = pcclass.MatrixCorrelation(dim=dim)
    for r in reals_points(
        shape=(dim * (dim - 1) // 2,), log2n=profile.R2P_LOG2N, wrap=array
    ):
        v = p.reals1d_to_params(r * rscale)
        assert v.shape == (dim, dim), f"r={r}"
        assert np.abs(v - v.T).max() < eps, f"r={r}"
        assert (np.linalg.eigh(v)[0]).min() > -dim * eps
        assert np.max(np.abs(np.diag(v) - 1)) < eps


@pytest.mark.parametrize(
    ("pcclass", "dim"), itertools.product(MODULES, MATRIX_SIZES_CORR)
)
def test_p2r_matrix_corr(profile, pcclass, dim):
    pcclass, array = pcclass
    p = pcclass.MatrixCorrelation(dim=dim)
    rs = RandomState(MT19937(SeedSequence(123456789)))

    for i in range(profile.P2R_N):
        v = rs.normal(size=(dim, dim))
        v = v @ v.T
        sd = np.sqrt(np.diag(v))
        v = v / sd[None, :] / sd[:, None]
        v = array(v)
        x = p.params_to_reals1d(v)
        assert functools.reduce(operator.mul, x.shape, 1) == dim * (dim - 1) // 2
        assert not np.isnan(x).any() and not np.isinf(x).any(), f"v={v}"
