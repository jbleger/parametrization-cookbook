import pytest
import itertools

import numpy as np
import jax.numpy as jnp
import torch

from parametrization_cookbook import numpy as pcnp
from parametrization_cookbook import jax as pcjax
from parametrization_cookbook import torch as pctorch

from .seq import reals_points, reals_points_norm_half

LOCS = (-10, 0, 10)
SCALES = (0.5, 1.0, 2.0)
ONE_BOUNDS = (-10, -5, 5, 10)
VECTOR_DIMS = (1, 4, 16)
VECTOR_DIMS_BALL = (2, 4, 8)
MATRIX_DIMS = (1, 2, 4, 6)

MODULES_EPS = (
    (pcnp, 1e-12, np.array),
    (pcjax, 1e-6, jnp.array),
    (pctorch, 1e-6, torch.tensor),
)

CLASSES_TO_TEST = (
    [(1, "Real", {"loc": loc, "scale": scale}) for loc in LOCS for scale in SCALES]
    + [(10, "RealPositive", {"scale": scale}) for scale in SCALES]
    + [(10, "RealNegative", {"scale": scale}) for scale in SCALES]
    + [
        (10, "RealLowerBounded", {"bound": bound, "scale": scale})
        for bound in ONE_BOUNDS
        for scale in SCALES
    ]
    + [
        (10, "RealUpperBounded", {"bound": bound, "scale": scale})
        for bound in ONE_BOUNDS
        for scale in SCALES
    ]
    + [(1, "RealBounded01", {})]
    + [
        (1, "RealBounded", {"bound_lower": b1, "bound_upper": b2})
        for b1, b2 in itertools.product(ONE_BOUNDS, ONE_BOUNDS)
        if b1 < b2
    ]
    + [(1, "VectorSimplex", {"dim": dim}) for dim in VECTOR_DIMS]
    + [(1, "VectorSphere", {"dim": dim}) for dim in VECTOR_DIMS]
    + [(1, "VectorHalfSphere", {"dim": dim}) for dim in VECTOR_DIMS]
    + [(10, "VectorBall", {"dim": dim}) for dim in VECTOR_DIMS_BALL]
    + [(1, "MatrixDiag", {"dim": dim}) for dim in MATRIX_DIMS]
    + [
        (1, "MatrixDiagPosDef", {"dim": dim, "scale": scale})
        for dim in MATRIX_DIMS
        for scale in SCALES
    ]
    + [(1, "MatrixSym", {"dim": dim}) for dim in MATRIX_DIMS]
    + [
        (1, "MatrixSymPosDef", {"dim": dim, "scale": scale})
        for dim in MATRIX_DIMS
        for scale in SCALES
    ]
    + [(1, "MatrixCorrelation", {"dim": dim}) for dim in MATRIX_DIMS if dim > 1]
)


print(CLASSES_TO_TEST, file=open("/tmp/toto", "w"))


@pytest.mark.parametrize(
    ("pcclass_eps", "classdesc"), itertools.product(MODULES_EPS, CLASSES_TO_TEST)
)
def test_bijective(profile, pcclass_eps, classdesc):
    factor_eps, classname, classargs = classdesc
    pcclass, eps, array = pcclass_eps
    p = getattr(pcclass, classname)(**classargs)

    for i, r in enumerate(
        array(x)
        for x in reals_points_norm_half(shape=(p.size,), log2n=profile.R2P_LOG2N)
    ):
        r2 = p.params_to_reals1d(p.reals1d_to_params(r))
        assert ((r - r2) ** 2).mean() ** 0.5 / max(
            1.0, np.abs(r).max()
        ) < p.size * eps * factor_eps, f"i={i}, p={p}, ramax={np.abs(r).max()}"
